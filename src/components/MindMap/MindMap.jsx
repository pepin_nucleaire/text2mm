/* eslint-disable react/prop-types */
import React, { useRef, useEffect } from 'react';
import { mindMapD3 } from '../../lib/mindMapD3';

const MindMap = ({
  data, getSize, getMargin, getTextSize,
}) => {
  const svgRef = useRef(null);
  const tooltipRef = useRef(null);

  useEffect(() => {
    if (data && svgRef.current && tooltipRef.current) {
      mindMapD3(data, svgRef.current, tooltipRef.current, getSize, getMargin, getTextSize);
    }
  }, [data, svgRef, tooltipRef, getSize, getMargin, getTextSize]);

  return (
    <section className="mindmap">
      <div ref={tooltipRef} />
      <svg ref={svgRef} />
    </section>
  );
};

export default MindMap;
