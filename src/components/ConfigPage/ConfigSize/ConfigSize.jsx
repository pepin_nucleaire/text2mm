/* eslint-disable react/prop-types */
import React from 'react';

const ConfigSize = ({ getSize, setSize }) => {
  const handleSizeChange = (e) => {
    setSize(e.target.value);
  };

  return (
    <div>
      Size
      <input type="range" min="200" max="2000" value={getSize} className="about__slider" onChange={handleSizeChange} />
    </div>
  );
};

export default ConfigSize;
