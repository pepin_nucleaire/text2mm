import React from 'react';

const HelpPage = () => (
  <section className="about">
    <h1>Help</h1>
    <p>The text2mm.com is Mind Map creation from tab indented text application.</p>
    <h2>Basic</h2>
    <p>You can edit text on left side.</p>
    <p>The first line is the name of the Mind Map root element.</p>
    <p>
      Following lines represents branches.
      You can change branch level by indenting lines using tabs.
    </p>
    <h2>Notes</h2>
    <p>You can add a note for each branch.</p>
    <p>
      Place your note after branch, between two ===.
      The first === starts note, second ends note.
    </p>
    <p>The content between === can be Markdown formatted text.</p>
    <p>Notes are visible when mouse over the corresponding branch.</p>
    <h2>Save</h2>
    <p>
      You can save Mind Map text in the browser&apos;s memory,
      by pressing Ctrl+s while editing Mind Map name or branches.
    </p>
    <p>You will see saved text next time you visit text2mm.com.</p>
    <p>
      Text is saved only in this particular browser and will be available
      until text2mm.com localstorage is cleared.
    </p>
    <h2>Configuration</h2>
    <p>Mind Map appearance can be adjusted using sliders in the configuration section.</p>
    <p>You can change Mind Map size, text size, margin size and color scheme.</p>
    <h2>Download</h2>
    <p>
      Mindmap text and Mind Map image in SVG format available for download.
      The SVG is without notes.
    </p>
    <h2>Suggestions and feedback</h2>
    <p>
      Contact me
      {' '}
      <a href="https://www.raibis.lt">www.raibis.lt</a>
    </p>
  </section>
);

export default HelpPage;
