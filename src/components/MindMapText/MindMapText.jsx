/* eslint-disable react/prop-types */
import React, { useState } from 'react';

const MindMapText = ({
  getName, setName, getText, setText,
}) => {
  const [getSave, setSave] = useState(false);

  const saveInLocalStorage = (e) => {
    // Ctrl+s Save handle
    if (e.ctrlKey && e.key === 's') {
      e.preventDefault();
      setSave(true);
      setTimeout(() => setSave(false), 3000);
      localStorage.setItem('name', getName);
      localStorage.setItem('text', getText);
    }
  };
  const nameChangetHandler = (e) => {
    setName(e.target.value);
  };

  const textChangeHandler = (e) => {
    setText(e.target.value);
  };

  const nameKeyDownHandler = (e) => {
    saveInLocalStorage(e);
  };

  const textKeyDownHandler = (e) => {
    saveInLocalStorage(e);
    if (e.key === 'Tab') {
      e.preventDefault();
      const { selectionStart, selectionEnd } = e.target;
      e.target.value = `${getText.substring(0, selectionStart)}\t${getText.substring(selectionEnd)}`;
      e.target.selectionStart = selectionStart + 1;
      e.target.selectionEnd = selectionEnd + 1;
      textChangeHandler(e);
    }
  };

  return (
    <section className="mindmaptext">
      {getSave && <div className="mindmaptext__saved">Text has been saved</div>}
      <input type="text" onKeyDown={nameKeyDownHandler} placeholder="MindMap name" onChange={nameChangetHandler} value={getName} />
      <textarea value={getText} onKeyDown={textKeyDownHandler} onChange={textChangeHandler} placeholder="MindMap text" />
    </section>
  );
};

export default MindMapText;
