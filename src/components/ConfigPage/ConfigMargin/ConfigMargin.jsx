/* eslint-disable react/prop-types */
import React from 'react';

const ConfigMargin = ({ getMargin, setMargin }) => {
  const handleMarginChange = (e) => {
    setMargin(e.target.value);
  };

  return (
    <div>
      Margin
      <input type="range" min="1" max="500" value={getMargin} className="about__slider" onChange={handleMarginChange} />
    </div>
  );
};

export default ConfigMargin;
