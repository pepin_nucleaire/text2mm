/* eslint-disable no-tabs */
/* eslint-disable no-undef */
const initialName = localStorage.getItem('name') || 'Text 2 Mind Map';
const initialText = localStorage.getItem('text')
  || `Why?
	Create Mind Maps blazing fast
	===
	Traditional Mind Map tools are created as drawing tools.

	Takes a lot of time to create larger MindMap.
	===
	No learning curve, trivial as Notepad
	Learn concepts faster
	===
	You can add Notes for each element.

	Describe concept in details
	===

What?
	Structure information and  your thoughts 
	Enhance your learning
	===
	The spatial layout helps you gain a better overview and makes new connections more visible so you can create an infinite number of thoughts, ideas, links, and associations on any topic.
	===
	Plan and Organize
	===
	Evidence shows that Mind Mapping can be used to help you plan and organize your thinking before you start writing or get stuck into a project. 

	You can develop all your ideas and see where they relate to each other before deciding the best way to go about things.
	===
	Problem Solving
	===
	A Mind Map can help you think with greater clarity to explore relationships between ideas and elements of an argument and to generate solutions to problems. It puts a new perspective on things by allowing you to see all the relevant issues and analyze choices in light of the big picture. It also makes it easier to integrate new knowledge and organize information logically as you aren’t tied to a rigid structure.
	===

How?
	Turn tab-indented lists into Mind Map
	Press Tab to indent lines
	Add notes describing in more details
	Place your note between === 
	Write your rich formatted notes
	Notes support markdown
	===
	# Heading 1
	## Heading 2
	### Heading 3
	#### Heading 4
	##### Heading 5
	###### Heading 6

	_italic_
	
	**bold**
	
	**_bold and  italic_**
	
	Add links [Contact developer](https://www.raibis.lt)
	
	Add images ![Mindmap](/mm.png)
	===
	Zoom in and out using a scroll
	Download
		Text as TXT
		Image as SVG`;

export { initialName, initialText };
